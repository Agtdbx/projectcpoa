package gameEngine;

import monde.Monde;

public class Game {

    private Display display;
    private Monde monde;
    private double nbFps;

    /*
    Contructeur du jeu
    Prends la taille en pixel de la fenêtre du jeu
    Les fps sont la vitesse de calcule de la simulation
    */
    public Game(int width, int height, double fps) {
        display = new Display(width, height);
        monde = Monde.getMonde(width / 50, height / 50 + 1);
        this.nbFps = fps;
    }

    /*
    Méthode de calcule du jeu
    */
    public void update() {
        monde.heureSuivante();
    }

    /*
    Méthode d'affichage du jeu
    */
    public void render() {
        display.render(this);
    }

    public Monde getMonde() {
        return monde;
    }

    public double getFps(){
        return nbFps;
    }
}
