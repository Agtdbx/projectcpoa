package gameEngine;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Display extends JFrame {

    private Canvas canvas;

    /*
    Contructeur de la fenêtre
    Prends la taille en pixel de la fenêtre
    */
    public Display(int width, int height) {
        setTitle("Little Simulation");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(width, height));
        canvas.setFocusable(false);
        add(canvas);
        pack();

        canvas.createBufferStrategy(3);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    /*
    Méthode d'affichage de la fenêtre
    */
    public void render(Game game) {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        Graphics graphics = bufferStrategy.getDrawGraphics();

        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        game.getMonde().render(graphics);

        graphics.dispose();
        bufferStrategy.show();
    }
}
