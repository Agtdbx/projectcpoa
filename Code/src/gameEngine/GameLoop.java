package gameEngine;

public class GameLoop implements Runnable {

    private Game game;

    private boolean running;
    private double updateRate = 1.0d / 12.0d;

    private long nextStatTime;
    private int fps, ups;

    /*
    Contructeur de la boucle de jeu
    */
    public GameLoop(Game game) {
        this.game = game;
        updateRate = 1.0d/game.getFps();
    }

    /*
    Méthode faisant tourner le jeu
    */
    @Override
    public void run() {
        running = true;
        double accumulator = 0;
        long currenTime, lastUpdate = System.currentTimeMillis();
        nextStatTime = System.currentTimeMillis() + 1000;

        while (running) {
            currenTime = System.currentTimeMillis();
            double lastRenderTimeInSeconds = (currenTime - lastUpdate) / 1000d;
            accumulator += lastRenderTimeInSeconds;
            lastUpdate = currenTime;

            while (accumulator > updateRate) {
                update();
                accumulator -= updateRate;
            }
            render();
            // printStats();
        }
    }

    /*
    Méthode de calcule
    */
    private void update() {
        game.update();
        ups++;
    }

    /*
    Méthode d'affichage
    */
    private void render() {
        game.render();
        fps++;
    }

    /*
    Méthode d'affichage des perfomances
    */
    private void printStats() {
        if (System.currentTimeMillis() > nextStatTime) {
            System.out.println(String.format("FPS: %d, UPS: %d", fps, ups));
            fps = 0;
            ups = 0;
            nextStatTime = System.currentTimeMillis() + 1000;
        }
    }
}
