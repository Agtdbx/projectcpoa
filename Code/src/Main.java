import java.awt.*;

import gameEngine.Game;
import gameEngine.GameLoop;

public class Main {
    public static void main(String args[]) {
        double fps = 2.0d; // C'est le nombre d'heure qui passe par seconde ! 24 fps = 1 journée pas secondes. Merci de laisser le 'd' à la fin du double

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int widht = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();
        
        new Thread(new GameLoop(new Game(widht, height-120, fps))).start();
    }
}