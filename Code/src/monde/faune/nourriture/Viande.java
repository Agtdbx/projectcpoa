package monde.faune.nourriture;

import java.awt.*;

public class Viande implements Aliment {

    private String nom;
    private int valeurEnergetique;
    private int age;
    private int ageMaxi;
    private boolean vivant;

    /*
    Constructeur de Viande
    */
    public Viande() {
        nom = "Viande";
        valeurEnergetique = 20;
        age = 0;
        ageMaxi = 5;
        vivant = true;
    }

    @Override
    public boolean estMort() {
        return !this.vivant;
    }

    /*
    Méthode gérant ce qui ce passe lorsqu'on grandit
    */
    @Override
    public void grandir() {
        age += 1;
        if (age >= ageMaxi) {
            vivant = false;
        }
    }

    @Override
    public boolean estAnimal() {
        return false;
    }

    /*
    Méthode gérant le jour suivant
    */
    @Override
    public void jourSuivant() {
        grandir();
    }

    /*
    Méthode gérant l'affichage
    */
    @Override
    public void render(Graphics graphics, int x, int y) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Image img = t.getImage("img/meat.png");
        graphics.drawImage(img, x, y, null);
    }

    @Override
    public String getNom() {
        return nom;
    }

    public int getEnergie() {
        return valeurEnergetique;
    }

    @Override
    public boolean estAdulte() {
        return true;
    }

    @Override
    public int getValeurEnergetique() {
        return valeurEnergetique;
    }
}
