package monde.faune.nourriture;

import java.awt.*;

public class Vegetaux implements Aliment {

    private String nom;
    private int valeurEnergetique;
    private int tempsMaturation;
    private int age;
    private int ageMaxi;
    private boolean vivant;

    /*
    Interface de Végétaux. 
    Prends en paramètre un nom et la valeur énergétique du végétaux
    */
    public Vegetaux(String nom, int valeurEnergetique) {
        this.nom = nom;
        this.valeurEnergetique = valeurEnergetique;
        tempsMaturation = 1;
        age = 0;
        ageMaxi = 5;
        vivant = true;
    }

    @Override
    public boolean estMort() {
        return !this.vivant;
    }

    /*
    Méthode gérant ce qui ce passe lorsque le temps passe
    */
    @Override
    public void grandir() {
        age += 1;
        if (age >= ageMaxi) {
            vivant = false;
        }
    }

    @Override
    public boolean estAnimal() {
        return false;
    }

    /*
    Méthode gérant les changements liés aux changement de jours
    */
    @Override
    public void jourSuivant() {
        grandir();
    }

    /*
    Méthode gérant l'affichage
    */
    @Override
    public void render(Graphics graphics, int x, int y) {
        if (nom == "Carotte"){
            if (age >= tempsMaturation) {
                Toolkit t = Toolkit.getDefaultToolkit();
                Image img = t.getImage("img/carotte.png");
                graphics.drawImage(img, x, y, null);
            } else {
                Toolkit t = Toolkit.getDefaultToolkit();
                Image img = t.getImage("img/carottePlant.png");
                graphics.drawImage(img, x, y, null);
            }
        } 
    }

    public String getNom() {
        return nom;
    }

    public int getEnergie() {
        return valeurEnergetique;
    }

    @Override
    public boolean estAdulte() {
        return age >= tempsMaturation;
    }

    @Override
    public int getValeurEnergetique() {
        return valeurEnergetique;
    }
}
