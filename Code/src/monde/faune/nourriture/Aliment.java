package monde.faune.nourriture;

import java.awt.*;

public interface Aliment {

    public boolean estMort();

    public void grandir();

    public boolean estAnimal();

    public void jourSuivant();

    public void render(Graphics graphics, int x, int y);

    public boolean estAdulte();

    public String getNom();

    public int getValeurEnergetique();
}
