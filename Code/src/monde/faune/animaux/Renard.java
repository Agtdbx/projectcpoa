package monde.faune.animaux;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

import monde.*;

public class Renard extends Animal {

    /*
    Constructeur du Renard.
    Prends en paramètre les coordonées de la tuile ou il se trouve ainsi que son age
    */
    public Renard(int x, int y, int age) {
        super(80, age, 40, 6, 300, 12, x, y);
        regime.addAll(Arrays.asList("Viande", "Lapin"));
        habitat.addAll(Arrays.asList("Prairie", "Foret"));
    }

    /*
    Méthode gérant l'affichage du Lapin
    */
    public void render(Graphics graphics, int x, int y) {
        if (estAdulte()) {
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/fox.png");
            graphics.drawImage(img, x, y, null);
        } else {
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/littleFox.png");
            graphics.drawImage(img, x, y, null);
        }
    }

    /*
    Méthode gérant l'acouchement du Renard
    */
    public void acouchement(Monde monde){
        List<List<Tuile>> carte = monde.getCarte();
        Renard renard = new Renard(posX, posY, 0);
        bebeEnCours = -1;
        energie -= 10;
        monde.ajouterAnimal(renard);
        carte.get(posX).get(posY).arriver(renard);
    }

    public String getNom() {
        return "Renard";
    }
}
