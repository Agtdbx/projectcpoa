package monde.faune.animaux.etat;

public class Faim implements IEtat {
    /*
    Etant représentant le faim de l'animal
    */
    public Faim() {
    }

    @Override
    public void mourir(Automate automate) {
        automate.changerEtat(new Mort());
    }

    @Override
    public void manger(Automate automate) {
        automate.changerEtat(new Rassasie());
    }

    @Override
    public void avoirFaim(Automate automate) {
    }

    @Override
    public void attendreMale(Automate automate){
        
    }

    @Override
    public void accoupler(Automate automate){

    }

    @Override
    public void voirFemelle(Automate automate){

    }

    @Override
    public String toString(){
        return "Faim";
    }
}
