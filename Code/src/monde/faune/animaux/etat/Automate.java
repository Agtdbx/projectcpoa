package monde.faune.animaux.etat;

public class Automate implements IEvenement {
    /*
    Classe se chargeant du stockage de l'état actuel et du passage d'un état à un autre
    */

    private IEtat etatCourant;

    public Automate(IEtat etat) {
        etatCourant = etat;
    }

    public void changerEtat(IEtat etat) {
        this.etatCourant = etat;
    }

    public String getEtatCourant() {
        return etatCourant.toString();
    }

    @Override
    public void mourir() {
        etatCourant.mourir(this);
    }

    @Override
    public void manger() {
        etatCourant.manger(this);
    }

    @Override
    public void avoirFaim() {
        etatCourant.avoirFaim(this);
    }

    @Override
    public void attendreMale(){
        etatCourant.attendreMale(this);
    }

    @Override
    public void accoupler(){
        etatCourant.accoupler(this);
    }

    @Override
    public void voirFemelle(){
        etatCourant.voirFemelle(this);
    }
}