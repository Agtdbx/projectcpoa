package monde.faune.animaux.etat;

public class Rassasie implements IEtat {
    /*
    Etant représentant le fait que l'animal n'a pas faim
    */
    public Rassasie() {
    }

    @Override
    public void mourir(Automate automate) {
        automate.changerEtat(new Mort());
    }

    @Override
    public void manger(Automate automate) {
    }

    @Override
    public void avoirFaim(Automate automate) {
        automate.changerEtat(new Faim());
    }

    @Override
    public void attendreMale(Automate automate){
        automate.changerEtat(new AttenteMale());
    }

    @Override
    public void accoupler(Automate automate){

    }

    @Override
    public void voirFemelle(Automate automate){
        automate.changerEtat(new VersFemelle());
    }

    @Override
    public String toString(){
        return "Rassasie";
    }
}
