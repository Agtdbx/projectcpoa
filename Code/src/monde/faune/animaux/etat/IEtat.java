package monde.faune.animaux.etat;

public interface IEtat {

    public void mourir(Automate automate);

    public void manger(Automate automate);

    public void avoirFaim(Automate automate);

    public void attendreMale(Automate automate);

    public void accoupler(Automate automate);

    public void voirFemelle(Automate automate);
}