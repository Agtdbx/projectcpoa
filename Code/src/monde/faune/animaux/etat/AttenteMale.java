package monde.faune.animaux.etat;

public class AttenteMale implements IEtat{
    /*
    Etant représentant l'attente du mâle par la female
    */
    public AttenteMale() {
    }

    @Override
    public void mourir(Automate automate) {
        automate.changerEtat(new Mort());
    }

    @Override
    public void manger(Automate automate) {
    }

    @Override
    public void avoirFaim(Automate automate) {
    }

    @Override
    public void attendreMale(Automate automate){
        
    }

    @Override
    public void accoupler(Automate automate){
        automate.changerEtat(new Rassasie());

    }

    @Override
    public void voirFemelle(Automate automate){

    }

    @Override
    public String toString(){
        return "AttenteMale";
    }
}
