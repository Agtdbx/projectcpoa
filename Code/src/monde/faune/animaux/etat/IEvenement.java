package monde.faune.animaux.etat;

public interface IEvenement {

    public void mourir();

    public void manger();

    public void avoirFaim();

    public void attendreMale();

    public void accoupler();

    public void voirFemelle();
}