package monde.faune.animaux.deplacement;

import monde.faune.animaux.Animal;
import monde.Monde;

public interface IDeplacement {

    public void deplacement(Monde monde, int x, int y, Animal animal);
}
