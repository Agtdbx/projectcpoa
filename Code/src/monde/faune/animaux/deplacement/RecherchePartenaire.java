package monde.faune.animaux.deplacement;

import java.util.List;
import java.util.Random;

import monde.faune.animaux.Animal;
import monde.*;

public class RecherchePartenaire implements IDeplacement {

     /*
    Méthode gérant la recherche de femelle par l'animal
    */
    @Override
    public void deplacement(Monde monde, int x, int y, Animal animal) {
        List<List<Tuile>> carte = monde.getCarte();
        if (!carte.get(x).get(y).yaFemelle(animal)) {
            if (moveIfGood(carte, x - 1, y, animal)) {}
            else if (moveIfGood(carte, x - 1, y - 1, animal)) {}
            else if (moveIfGood(carte, x, y - 1, animal)) {}
            else if (moveIfGood(carte, x + 1, y - 1, animal)) {}
            else if (moveIfGood(carte, x + 1, y, animal)) {}
            else if (moveIfGood(carte, x + 1, y + 1, animal)) {}
            else if (moveIfGood(carte, x, y + 1, animal)) {}
            else if (moveIfGood(carte, x - 1, y + 1, animal)) {}
            
            else if (moveIfNextGood(carte, x - 2, y, animal)) {}
            else if (moveIfNextGood(carte, x - 2, y - 1, animal)) {}
            else if (moveIfNextGood(carte, x - 2, y - 2, animal)) {}
            else if (moveIfNextGood(carte, x - 1, y - 2, animal)) {}
            else if (moveIfNextGood(carte, x, y - 2, animal)) {}
            else if (moveIfNextGood(carte, x + 1, y - 2, animal)) {}
            else if (moveIfNextGood(carte, x + 2, y - 2, animal)) {}
            else if (moveIfNextGood(carte, x + 2, y - 1, animal)) {}
            else if (moveIfNextGood(carte, x + 2, y, animal)) {}
            else if (moveIfNextGood(carte, x + 2, y + 1, animal)) {}
            else if (moveIfNextGood(carte, x + 2, y + 2, animal)) {}
            else if (moveIfNextGood(carte, x + 1, y + 2, animal)) {}
            else if (moveIfNextGood(carte, x, y + 2, animal)) {}
            else if (moveIfNextGood(carte, x - 1, y + 2, animal)) {}
            else if (moveIfNextGood(carte, x - 2, y + 2, animal)) {}
            else if (moveIfNextGood(carte, x - 2, y + 1, animal)) {}
            
            else {
                Random r = new Random();
                for (int i = 0; i < 10; i++) {
                    int random = r.nextInt(7);
                    if (random == 0 && moveIfPossible(carte, x - 1, y, animal)) {break;}
                    else if (random == 1 && moveIfPossible(carte, x - 1, y - 1, animal)) {break;}
                    else if (random == 2 && moveIfPossible(carte, x, y - 1, animal)) {break;}
                    else if (random == 3 && moveIfPossible(carte, x + 1, y - 1, animal)) {break;}
                    else if (random == 4 && moveIfPossible(carte, x + 1, y, animal)) {break;}
                    else if (random == 5 && moveIfPossible(carte, x + 1, y + 1, animal)) {break;}
                    else if (random == 6 && moveIfPossible(carte, x, y + 1, animal)) {break;}
                    else if (random == 7 && moveIfPossible(carte, x - 1, y + 1, animal)) {break;}
                }
            }
        }
    }

    /*
    Méthode permettant de savoir s'il est préférable de se déplace sur cette tuile
    */
    private boolean moveIfGood(List<List<Tuile>> carte, int x, int y, Animal animal) {
        if (0 <= x && x < carte.size() && 1 <= y && y < carte.get(0).size()
                && animal.estVisitable(carte.get(x).get(y)) && carte.get(x).get(y).yaFemelle(animal)) {
            carte.get(animal.getX()).get(animal.getY()).partir(animal);
            animal.setCoordonnee(x, y);
            carte.get(x).get(y).arriver(animal);
            return true;
        }
        return false;
    }

    /*
    Méthode permettant de savoir s'il est préférable de se déplace sur cette tuile pour attendre la femelle au tour d'après
    */
    private boolean moveIfNextGood(List<List<Tuile>> carte, int x, int y, Animal animal){
        if (0 <= x && x < carte.size() && 1 <= y && y < carte.get(0).size()
                && animal.estVisitable(carte.get(x).get(y)) && carte.get(x).get(y).yaFemelle(animal)) {
            int movX = x - animal.getX();
            int movY = y - animal.getY();
            if (movX < 0 && movY == 0){
                moveIfPossible(carte, x - 1, y, animal);
            }
            else if (movX < 0 && movY < 0){
                moveIfPossible(carte, x - 1, y - 1, animal);
                
            }
            else if (movX == 0 && movY < 0){
                moveIfPossible(carte, x, y - 1, animal);
            }
            else if (movX > 0 && movY < 0){
                moveIfPossible(carte, x + 1, y - 1, animal);
            }
            else if (movX > 0 && movY == 0){
                moveIfPossible(carte, x + 1, y, animal);
            }
            else if (movX > 0 && movY > 0){
                moveIfPossible(carte, x + 1, y + 1, animal);;
            }
            else if (movX == 0 && movY > 0){
                moveIfPossible(carte, x, y + 1, animal);
            }
            else if (movX < 0 && movY > 0){
                moveIfPossible(carte, x - 1, y + 1, animal);
            }
            return true;
        }
        return false;
    }

    /*
    Méthode permettant de savoir si on peut se déplace sur cette tuile
    */
    private boolean moveIfPossible(List<List<Tuile>> carte, int x, int y, Animal animal) {
        if (0 <= x && x < carte.size() && 1 <= y && y < carte.get(0).size()
                && animal.estVisitable(carte.get(x).get(y))) {
            carte.get(animal.getX()).get(animal.getY()).partir(animal);
            animal.setCoordonnee(x, y);
            carte.get(x).get(y).arriver(animal);
            return true;
        }
        return false;
    }
}
