package monde.faune.animaux.deplacement;

import monde.faune.animaux.Animal;
import monde.Monde;

public class Attente implements IDeplacement {
    /*
    Méthode gérant l'attente de l'animal
    */
    @Override
    public void deplacement(Monde monde, int x, int y, Animal animal) {
    }
}
