package monde.faune.animaux.deplacement;

import java.util.List;

import monde.faune.animaux.Animal;
import monde.*;

public class goToFemelle implements IDeplacement{

    /*
    Méthode gérant le déplacement vers la femelle choisi pour la reproduction
    */
    @Override
    public void deplacement(Monde monde, int x, int y, Animal animal) {
        List<List<Tuile>> carte = monde.getCarte();
        if (!carte.get(x).get(y).yaFemelle(animal)) {
            if (moveIfGood(carte, x - 1, y, animal)) {}
            else if (moveIfGood(carte, x - 1, y - 1, animal)) {}
            else if (moveIfGood(carte, x, y - 1, animal)) {}
            else if (moveIfGood(carte, x + 1, y - 1, animal)) {}
            else if (moveIfGood(carte, x + 1, y, animal)) {}
            else if (moveIfGood(carte, x + 1, y + 1, animal)) {}
            else if (moveIfGood(carte, x, y + 1, animal)) {}
            else if (moveIfGood(carte, x - 1, y + 1, animal)) {}
        }
    }

    /*
    Méthode permettant de savoir s'il est préférable de se déplace sur cette tuile
    */
    private boolean moveIfGood(List<List<Tuile>> carte, int x, int y, Animal animal) {
        if (0 <= x && x < carte.size() && 1 <= y && y < carte.get(0).size()
                && animal.estVisitable(carte.get(x).get(y)) && carte.get(x).get(y).yaFemelleCible(animal)) {
            carte.get(animal.getX()).get(animal.getY()).partir(animal);
            animal.setCoordonnee(x, y);
            carte.get(x).get(y).arriver(animal);
            return true;
        }
        return false;
    }
}
