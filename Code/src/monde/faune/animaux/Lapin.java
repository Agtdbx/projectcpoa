package monde.faune.animaux;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

import monde.*;

public class Lapin extends Animal {

    /*
    Constructeur du Lapin.
    Prends en paramètre les coordonées de la tuile ou il se trouve ainsi que son age
    */
    public Lapin(int x, int y, int age) {
        super(50, age, 40, 5, 40, 4, x, y);
        regime.addAll(Arrays.asList("Carotte"));
        habitat.addAll(Arrays.asList("Prairie"));
    }

    /*
    Méthode gérant l'affichage du Lapin
    */
    public void render(Graphics graphics, int x, int y) {
        if (estAdulte()) {
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/bunny.png");
            graphics.drawImage(img, x, y, null);
        } else {
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/littleBunny.png");
            graphics.drawImage(img, x, y, null);
        }
    }

    /*
    Méthode gérant l'acouchement du Lapin
    */
    public void acouchement(Monde monde){
        List<List<Tuile>> carte = monde.getCarte();
        Lapin lapin = new Lapin(posX, posY, 0);
        bebeEnCours = -1;
        energie -= 10;
        monde.ajouterAnimal(lapin);
        carte.get(posX).get(posY).arriver(lapin);
    }

    public String getNom() {
        return "Lapin";
    }
}
