package monde.faune.animaux.action;

import java.util.List;

import monde.faune.animaux.Animal;
import monde.*;

public class Manger implements IAction {

    /*
    Méthode gérant l'action de manger d'un animal
    */
    @Override
    public void action(Monde monde, int x, int y, Animal animal) {
        List<List<Tuile>> carte = monde.getCarte();
        animal.manger(carte.get(x).get(y).manger(monde, animal));
    }
}
