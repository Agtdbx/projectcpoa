package monde.faune.animaux.action;

import java.util.List;

import monde.faune.animaux.Animal;
import monde.*;

public class Accoupler implements IAction {

    /*
    Méthode gérant l'accouplement d'un animal
    */
    @Override
    public void action(Monde monde, int x, int y, Animal animal) {
        List<List<Tuile>> carte = monde.getCarte();
        if (carte.get(x).get(y).yaFemelleCible(animal)){
            animal.reproduction();
        }
    }
}