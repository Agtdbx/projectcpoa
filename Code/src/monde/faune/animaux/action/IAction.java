package monde.faune.animaux.action;

import monde.faune.animaux.Animal;
import monde.Monde;

public interface IAction {

    public void action(Monde monde, int x, int y, Animal animal);
}
