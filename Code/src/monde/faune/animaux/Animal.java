package monde.faune.animaux;

import java.util.List;
import java.util.ArrayList;
import java.awt.*;
import java.util.Random;

import monde.faune.animaux.etat.*;
import monde.faune.animaux.deplacement.*;
import monde.faune.animaux.action.*;
import monde.*;
import monde.faune.nourriture.Aliment;

public abstract class Animal implements Aliment {

    protected int energie;
    protected int limiteEnergie;
    protected boolean sexe;
    protected int age;
    protected int ageMaxi;
    protected int tempsMaturation;
    protected int valeurEnergetique;
    protected int posX;
    protected int posY;
    protected List<String> regime;
    protected List<String> habitat;
    protected Automate automate;
    protected IDeplacement deplacement;
    protected IAction action;
    protected int tempsGestation;
    protected int bebeEnCours;
    protected Animal femelleCible;

    /*
    Constructeur d'Animal.
    Prends en paramètre la limite d'énergie, l'age, l'age maximal, le temps pour devenir adulte, sa valeur énergétique, le temps de gestation, sa position en x et y
    */
    public Animal(int limiteEnergie, int age, int ageMaxi, int tempsMaturation, int valeurEnergetique, int tempsGestation, int x, int y) {
        this.energie = limiteEnergie/2;
        this.limiteEnergie = limiteEnergie;
        Random r = new Random();
        if (r.nextInt(2) == 0){
            this.sexe = true;
        }
        else{
            this.sexe = false;
        }
        this.age = age;
        this.ageMaxi = ageMaxi;
        this.tempsMaturation = tempsMaturation;
        this.valeurEnergetique = valeurEnergetique;
        this.tempsGestation = tempsGestation;
        this.bebeEnCours = -1;
        this.femelleCible = null; 
        this.posX = x;
        this.posY = y;
        automate = new Automate(new Rassasie());
        deplacement = new Attente();
        action = new Reposer();
        regime = new ArrayList<>();
        habitat = new ArrayList<>();
    }

    /*
    Méthode gérant ce qui ce passe lorsque le temps passe
    */
    public void grandir() {
        age += 1;
        if (age >= ageMaxi) {
            automate.mourir();
        }
    }

    /*
    Méthode gérant le choix du déplacement de l'animal
    */
    public void seDeplacer(Monde monde) {
        switch (automate.getEtatCourant()) {
            case "Faim":
                deplacement = new RechercheManger();
                break;
            case "Rassasie":
                if (sexe && estAdulte()){
                    deplacement = new RecherchePartenaire();
                }
                else{
                    deplacement = new Explorer();
                }
                break;
            
            case "AttendreMale":
                deplacement = new Attente();
                break;
            
            case "VersFemelle":
                deplacement = new goToFemelle();
                break;
        }
        deplacement.deplacement(monde, posX, posY, this);
    }

    /*
    Méthode gérant le choix d'action de l'animal
    */
    public void effectuerAction(Monde monde) {
        switch (automate.getEtatCourant()) {
            case "Faim":
                action = new Manger();
                break;
            case "Rassasie":
                if (estAdulte()){
                    action = new Reposer();
                }
                else{
                    action = new Jouer();
                }
                break; 
            case "AttendreMale":
                action = new Reposer();
                break;
            case "VersFemelle":
                action = new Accoupler();
                break;
        }
        action.action(monde, posX, posY, this);
    }

    /*
    Méthode gérant ce qu'il se passe lorsqu'on change d'heure
    */
    public void heureSuivante(Monde monde) {
        energie--;
        if (energie < limiteEnergie / 2) {
            automate.avoirFaim();
        }
        if (energie <= 0) {
            automate.mourir();
        }
        if (bebeEnCours >= 0){
            bebeEnCours ++;
        }
        if (bebeEnCours >= tempsGestation){
            acouchement(monde);
        }
        if (!estMort()) {
            seDeplacer(monde);
            effectuerAction(monde);
        }
    }

    /*
    Méthode permetant de savoir si une femellet est disponible pour la reproduction
    */
    public boolean estDisponible(Animal male){
        if (getNom() == male.getNom()){
            if (male.getSexe() && !sexe && male.estAdulte() && estAdulte() && bebeEnCours < 0 && automate.getEtatCourant() != "AttenteMale"){
                automate.attendreMale();
                if (automate.getEtatCourant() == "AttenteMale"){
                    male.setFemelle(this);                    
                    return true;
                }
            }            
        }
        return false;
    }

    /*
    Méthode gérant le fait que la femelle devienne enceinte
    */
    public void femelleEnceinte(){
        bebeEnCours = 0;
        automate.accoupler();
    }

    /*
    Méthode gérant la reproduction
    */
    public void reproduction(){
        femelleCible.femelleEnceinte();
        energie -= 10;
        femelleCible = null;
        automate.accoupler();
    }

    /*
    Méthode gérant l'accouchement
    */
    public void acouchement(Monde monde){
        
    }

    /*
    Méthode gérant l'action de manger (Du côte recevoir l'énergie après avoir mangé)
    */
    public void manger(int miam) {
        energie += miam;
        if (energie >= limiteEnergie/2){
            automate.manger();
        }
        else if (energie > limiteEnergie) {
            energie = limiteEnergie;
        }        
    }

    /*
    Méthode gérant le jour suivant
    */
    @Override
    public void jourSuivant() {
        grandir();
    }

    /*
    Méthode gérant l'affichage de l'animal
    */
    @Override
    public void render(Graphics graphics, int x, int y) {

    }

    public int getX() {
        return posX;
    }

    public int getY() {
        return posY;
    }

    @Override
    public boolean estAdulte() {
        return age >= tempsMaturation;
    }

    public void setCoordonnee(int x, int y) {
        if (posX != x || posY != y) {
            energie--;
        }
        posX = x;
        posY = y;
    }

    public List<String> getRegime() {
        return regime;
    }

    @Override
    public String getNom() {
        return null;
    }

    @Override
    public int getValeurEnergetique() {
        return valeurEnergetique;
    }

    public boolean estVisitable(Tuile tuile) {
        return habitat.contains(tuile.getNom());
    }

    public boolean getSexe(){
        return sexe;
    }

    public void setFemelle(Animal femelle){
        femelleCible = femelle;
        automate.voirFemelle();
    }

    public Animal getFemelle(){
        return femelleCible;
    }

    @Override
    public String toString(){
        return this.getNom() + " " + age + " " + sexe;
    }

    public boolean estMort() {
        return automate.getEtatCourant() == "Mort";
    }

    public boolean estAnimal() {
        return true;
    }
}