package monde;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.awt.*;

import monde.faune.animaux.*;

public class Monde {

    private static Monde monde;
    private int heure;
    private int jour;
    private List<List<Tuile>> carte;
    private List<Animal> animals, nouveauAnimaux, animauxMort;

    /*
    Constructeur du monde.
    Prends en paramètre le nombre de tuile en x et en y
    */
    private Monde(int width, int height) {
        carte = new ArrayList<>();
        animals = new ArrayList<>();
        genererCarte(width, height);
        jourSuivant();
        jourSuivant();
        genererLapins(20, width, height);
        genererRenard(5, width, height);
    }

    /*
    Getter du singloton.
    Retourne le seul Monde qu'il est possible de créer
    */
    public static Monde getMonde(int width, int height) {
        if (monde == null) {
            monde = new Monde(width, height);
        }
        return monde;
    }

    /*
    Méthode générant les lapins
    */
    private void genererLapins(int nbLapin, int width, int height){
        Random r = new Random();
        for (int i = 0; i < nbLapin; i++) {
            for (int j = 0; j < 10; j++) {
                int x = r.nextInt(width - 1) + 1;
                int y = r.nextInt(height - 1);
                if (carte.get(x).get(y).getNom() == "Prairie") {
                    Lapin lapin = new Lapin(x, y, 6);
                    animals.add(lapin);
                    carte.get(x).get(y).arriver(lapin);
                    break;
                }
            }
        }
    }

    /*
    Méthode générant les renards
    */
    private void genererRenard(int nbRenard, int width, int height){
        Random r = new Random();
        for (int i = 0; i < nbRenard; i++) {
            for (int j = 0; j < 10; j++) {
                int x = r.nextInt(width - 1) + 1;
                int y = r.nextInt(height - 1);
                if (carte.get(x).get(y).getNom() == "Foret") {
                    Renard renard = new Renard(x, y, 6);
                    animals.add(renard);
                    carte.get(x).get(y).arriver(renard);
                    break;
                }
            }
        }
    }

    /*
    Méthode générant la carte avec des prairies partout
    */
    private void genererCarte(int width, int height) {
        for (int i = 0; i < width; i++) {
            List<Tuile> list = new ArrayList<>();
            for (int j = 0; j < height - 1; j++) {
                Tuile tuile = new Tuile("Prairie");
                list.add(tuile);
            }
            carte.add(list);
        }

        Random r = new Random();
        int nbForet = r.nextInt(3 - 1) + 1;
        for (int i = 0; i < nbForet; i++) {
            int x = r.nextInt(width - 1) + 1;
            int y = r.nextInt(height - 1);
            addForet(x, y);
        }

        int nbLac = r.nextInt(3 - 1) + 1;
        for (int i = 0; i < nbLac; i++) {
            int x = r.nextInt(width - 1) + 1;
            int y = r.nextInt(height - 1);
            addLac(x, y);
        }   
    }

    /*
    Méthode générant les lacs
    */
    private void addLac(int x, int y) {
        replaceIfPossible(x - 3, y + 3, "Lac");

        for (int i = 1; i < 5; i++) {
            replaceIfPossible(x - 2, y + i, "Lac");
        }

        for (int i = -1; i < 5; i++) {
            replaceIfPossible(x - 1, y + i, "Lac");
        }

        for (int i = -2; i < 4; i++) {
            replaceIfPossible(x, y + i, "Lac");
        }

        for (int i = -3; i < 3; i++) {
            replaceIfPossible(x + 1, y + i, "Lac");
        }

        for (int i = -3; i < 1; i++) {
            replaceIfPossible(x + 2, y + i, "Lac");
        }

        for (int i = -4; i < 0; i++) {
            replaceIfPossible(x + 3, y + i, "Lac");
        }

        replaceIfPossible(x + 4, y - 4, "Lac");
        replaceIfPossible(x + 4, y - 3, "Lac");
    }

    /*
    Méthode générant les forêts
    */
    private void addForet(int x, int y) {
        for (int i = -1; i < 3; i++) {
            replaceIfPossible(x - 5, y + i, "Foret");
        }
        for (int i = -3; i < 5; i++) {
            replaceIfPossible(x - 4, y + i, "Foret");
        }
        for (int i = -4; i < 6; i++) {
            replaceIfPossible(x - 3, y + i, "Foret");
        }
        for (int i = -4; i < 6; i++) {
            replaceIfPossible(x - 2, y + i, "Foret");
        }
        for (int i = -5; i < 7; i++) {
            replaceIfPossible(x - 1, y + i, "Foret");
        }
        for (int i = -5; i < 7; i++) {
            replaceIfPossible(x, y + i, "Foret");
        }
        for (int i = -5; i < 7; i++) {
            replaceIfPossible(x + 1, y + i, "Foret");
        }
        for (int i = -5; i < 7; i++) {
            replaceIfPossible(x + 2, y + i, "Foret");
        }
        for (int i = -4; i < 6; i++) {
            replaceIfPossible(x + 3, y + i, "Foret");
        }
        for (int i = -4; i < 6; i++) {
            replaceIfPossible(x + 4, y + i, "Foret");
        }
        for (int i = -3; i < 5; i++) {
            replaceIfPossible(x + 5, y + i, "Foret");
        }
        for (int i = -1; i < 3; i++) {
            replaceIfPossible(x + 6, y + i, "Foret");
        }
    }
    
    /*
    Méthode regardant si on peut remplacer une tuile par une autre
    */
    private void replaceIfPossible(int x, int y, String nom) {
        if (0 <= x && x < carte.size() && 1 <= y && y < carte.get(0).size()) {
            carte.get(x).set(y, new Tuile(nom));
        }
    }

    /*
    Méthode gérant ce qui ce passe lorsqu'on change de jour
    */
    public void jourSuivant() {
        for (int i = 0; i < carte.size(); i++) {
            for (int j = 0; j < carte.get(0).size(); j++) {
                carte.get(i).get(j).jourSuivant();
            }
        }
    }

    /*
    Méthode génrant ce qui ce passe lorsqu'on avance d'une heure
    */
    public void heureSuivante() {
        nouveauAnimaux = new ArrayList<>();
        animauxMort = new ArrayList<>();
        heure += 1;
        if (heure >= 24) {
            heure = 0;
            jour += 1;
            jourSuivant();
        }
        for (Animal a : animals) {
            a.heureSuivante(this);
            if (a.estMort()) {
                animauxMort.add(a);
            }
        }
        for (Animal a : animauxMort) {
            carte.get(a.getX()).get(a.getY()).toViande(a);
            animals.remove(a);
        }
        animals.addAll(nouveauAnimaux);
    }

    /*
    Méthode génrant l'affichage du monde
    */
    public void render(Graphics graphics) {
        for (int i = 0; i < carte.size(); i++) {
            for (int j = 1; j < carte.get(0).size(); j++) {
                carte.get(i).get(j).render(graphics, i, j);
            }
        }
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 1920, 50);
        graphics.setColor(Color.BLACK);
        graphics.drawString("Jour " + jour, 10, 20);
        graphics.drawString("Heure " + heure + "h00", 10, 40);
    }

    public List<List<Tuile>> getCarte() {
        return carte;
    }

    public void ajouterAnimal(Animal animal){ 
        nouveauAnimaux.add(animal);
    }

    public void estManger(Animal animal){
        animauxMort.add(animal);
    }
}