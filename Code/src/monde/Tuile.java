package monde;

import java.util.List;
import java.util.ArrayList;
import java.awt.*;

import monde.faune.nourriture.Aliment;
import monde.faune.animaux.Animal;
import monde.faune.nourriture.*;

public class Tuile {

    private List<Aliment> aliments;
    private List<Animal> animals;
    private String nom;
    
    /*
    Contructeur de la Tuile.
    Prends en paramètre sont nom
    */
    public Tuile(String nom) {
        this.nom = nom;
        aliments = new ArrayList<>();
        animals = new ArrayList<>();
    }
    

    /*
    Méthode gérant ce qui ce passe quand un jour passe
    */
    public void jourSuivant() {
        List<Aliment> removeDead = new ArrayList<>();
            for (Aliment a : aliments) {
                a.jourSuivant();
                if (a.estMort()) {
                    removeDead.add(a);
                }
            }
            for (Aliment a : removeDead) {
                aliments.remove(a);
            }
        if (nom == "Prairie") {
            double nb = Math.random() * 100;
            if (nb > 90) {
                Vegetaux vegetal = new Vegetaux("Carotte", 20);
                aliments.add(vegetal);
            }
        }
    }

    /*
    Méthode d'affichage
    */
    public void render(Graphics graphics, int x, int y) {
        if (nom == "Prairie") {
            // graphics.setColor(new Color(0, 200, 0));
            // graphics.fillRect(x * 50, y * 50, 50, 50);
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/grass.png");
            graphics.drawImage(img, x * 50, y * 50, null);
        } else if (nom == "Lac") {
            // graphics.setColor(new Color(70, 70, 230));
            // graphics.fillRect(x * 50, y * 50, 50, 50);
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/water.png");
            graphics.drawImage(img, x * 50, y * 50, null);
        } else if (nom == "Foret") {
            // graphics.setColor(new Color(0, 130, 0));
            // graphics.fillRect(x * 50, y * 50, 50, 50);
            Toolkit t = Toolkit.getDefaultToolkit();
            Image img = t.getImage("img/forest.png");
            graphics.drawImage(img, x * 50, (y * 50) - 26, null);
        }else {
            graphics.setColor(Color.WHITE);
            graphics.fillRect(x * 50, y * 50, 50, 50);
        }
        // graphics.setColor(Color.BLACK);
        // graphics.drawRect(x * 50, y * 50, 50, 50);
        drawVegetaux(graphics, x * 50, y * 50);
        drawAnimaux(graphics, x * 50, y * 50);
    }

    /*
    Méthode d'affichage des végétaux
    */
    private void drawVegetaux(Graphics graphics, int x, int y) {
        int i = 0;
        int j = 0;
        for (Aliment a : aliments) {
            if (!a.estAnimal()){
                if (i % 3 == 0 && i != 0) {
                    j += 1;
                }
                if (j > 2) {
                    break;
                }
                aliments.get(i).render(graphics, x + (i % 3) * 15 + 2, y + j * 15 + 2);
                i++;
            }
        }
    }

    /*
    Méthode d'affichage des animaux
    */
    private void drawAnimaux(Graphics graphics, int x, int y) {
        int j = 0;
        for (int i = 0; i < animals.size(); i++) {
            if (i % 2 == 0 && i != 0) {
                j += 1;
            }
            if (j > 1) {
                break;
            }
            animals.get(i).render(graphics, x + (i % 2) * 22 + 2, y + j * 22 + 2);
        }
    }

    /*
    Méthode gérant l'ajout d'un animal quand il arrive sur la tuile
    */
    public void arriver(Animal animal) {
        animals.add(animal);
        aliments.add(animal);
    }

    /*
    Méthode gérant la supression d'un animal quand il part de la tuile
    */
    public void partir(Animal animal) {
        animals.remove(animal);
        aliments.remove(animal);
    }

    /*
    Méthode pour savoir s'il reste de la place sur la tuile pour un animal
    */
    public boolean assezPlace() {
        return animals.size() < 4;
    }

    /*
    Méthode retournant le nombre truc à manger pour l'animal en paramètre
    */
    public int getNbAliment(Animal animal) {
        int nb = 0;
        for (Aliment a : aliments) {
            if (a.estAdulte() && animal.getRegime().contains(a.getNom())) {
                nb++;
            }
        }
        return nb;
    }

    /*
    Méthode gérant la mort d'un animal (Transformation de sa dépouille en viande)
    */
    public void toViande(Animal animal) {
        partir(animal);
        if (aliments.size() < 9) {
            Aliment viande = new Viande();
            aliments.add(viande);
        }
    }

    /*
    Méthode gérant la mort d'un animal quand il est mangé
    */
    public int manger(Monde monde, Animal animal) {
        int miam = -1;
        for (int i = 0; i < aliments.size(); i++) {
            if (animal.getRegime().contains(aliments.get(i).getNom()) && aliments.get(i).estAdulte()) {
                miam = i;
                break;
            }
        }
        if (miam < 0) {
            return 0;
        } else {
            Aliment gouter = aliments.get(miam);
            aliments.remove(miam);
            if (gouter.estAnimal()){
                monde.estManger((Animal)gouter);
            }
            return gouter.getValeurEnergetique();
        }
    }

    /*
    Méthode permettant de savoir s'il y'a des femelles disponible
    */
    public boolean yaFemelle(Animal animal){
        for (Animal a : animals){
            if (a.estDisponible(animal)){
                return true;
            }
        }
        return false;
    }

    /*
    Méthode permettant de savoir s'il y'a la femelle choisi pour se reproduire
    */
    public boolean yaFemelleCible(Animal animal){
        return animals.contains(animal.getFemelle());
    }

    public String getNom() {
        return nom;
    }

    public List<Animal> getAnimaux(){
        return animals;
    }
}
